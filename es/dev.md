---
title: Desarrollo
description:
published: 1
date: 2020-03-10T21:12:58.397Z
tags:
---

+ [Construyendo](/dev/building)
+ [ Camino a ser un mantenedor oficial](https://wiki.orangefox.tech/en/dev/maintainerships)
+ [Directrices para los mantenedores oficiales](/dev/maintainers_guidelines)
+ [API](/dev/api)
+ [Infraestructura de OrangeFox](/dev/infrastructure)
+ [Desarrollo de temas](/dev/theme_dev)
{.links-list}