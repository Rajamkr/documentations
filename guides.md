---
title: Guides and Frequently Asked Questions
description: 
published: 1
date: 2020-04-29T20:18:56.154Z
tags: 
---

- [Installing OrangeFox Recovery](installing_orangefox)
- [Basic questions](base)
- [Flashing](flashing)
- [Backups](backups)
- [MIUI / Flyme OTA](ota)
- [Encryption](encryption)
- [Others](others)
{.links-list}

