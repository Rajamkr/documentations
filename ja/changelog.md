---
title: OrangeFox 更新履歴
description:
published: 1
date: 2020-03-05T17:42:56.032Z
tags:
---

## OrangeFox R11
> In heavy development. ETA in 2020 
> 
> {.is-info}
* Match Android Q (Ten) design guidelines
* Android Q gestures
* Better UX
* Magisk manager improvements and fixes
* Now battery icon drawn by C code
* Merged latest TWRP changes

## OrangeFox R10.1 - 現在の最新版
* Save historic logs (compressed zips) - to the /sdcard/Fox/logs/ folder
* Now all logs will be placed in /sdcard/Fox/logs/
* Added support for incremental MIUI OTA on newer devices (eg, lavender, violet)
* Better support for system-as-root
* Improved support for Android 10
* Improved support for MIUI 11
* The default setting for the "Disable DM-Verity" and "Disable Forced-Encryption" boxes is to untick them. This is necessary because, first, the latest stock MIUI ROMs based on **Android Pie** or **Android 10** are not happy at all with disabling DM"-Verity" (incremental MIUI OTA updates will fail, and there may be other issues); secondly, the recent stock MIUI ROMs may encrypt your device anyway, and ignore all settings to stop this behaviour; thirdly, some recent AOSP-based ROMs are not happy at all with disabling DM-Verity or Forced-Encryption. Thus, anybody who chooses to enable these settings must tick those boxes manually before flashing ROMs (maintainers for very old devices may have kept the original defaults, because old devices with MIUI based on older Android releases do not have this problem)
* The Magisk addon has been updated (now Magisk v20.1)
* Handle "Android Rescue Party" messages
* More detailed information is now provided about the installed ROM (if any)
* Harmonise some sources with stock twrp
* Encryption: don't try wrapped key if not needed
* Encryption: cryptfs: add support for keymaster 2
* Fixed issues with app manager
* Disabled some unnecessary warnings for custom roms
* Added Ukrainian, French and Vietnamese languages
* Updated all other languages
* Lots of little fixes

## OrangeFox R10
* Fully refreshed design to Material Design 2 Guidelines
* OrangeFox team rethought logic of recovery
* Merged latest TWRP 3.3.1 commits
* Added inbuilt ROM app manager
* Added inbuilt Magisk Manger (No more bootloops by wrong modules!)
* Flashlight code updated to support more devices
* Added Quick backup option (to backup essential partitions with one click)
* Added support for OTA of non-MIUI devices
* MIUI OTA updated with many fixes
* Added PIN Code security option
* Added Gesture key security option
* Significantly increased the persistence of OrangeFox protection. The password will not reset even after reinstalling OrangeFox.
* Added ability to block ADB on startup.
* Significant upgrades to the built-in File Manager
* File manager supports the "back" key to go into the parent directories
* Installing ZIPs merged with File Manager
* Added ZIP, IMG files sort in File Manager
* To see the available actions for a file, long press it
* File Manager shows path to current directory
* Added search in File Manager
* Added txt files read ability in File Manager
* Added ability to Compress/Unpack in File Manager
* Added Checksum checker in File Manager
* Added Information page in file action
* Added ability to set premissions in File Manager
* Added hardware navigation key support
* Vol+ and power key for enable flashlight
* Added EDL reboot option
* Removed hard coded MIUI OTA, we can build OrangeFox without it for non-Xiaomi devices
* Added tons of customisation, new themes, colors, settings, battery styles, navbar settings, and many more.
* Themes now apply MUCH faster and keep after OrangeFox reinstallation
* Format data and partition SD merged to the new "Manage Partitions" page.
* The "Wipe" menu contains only wipe actions, and no more.
* To launch AromaFM you need click twice (mo more accidental opening of AromaFM)
* Backups and Restores merged in one page (to add backups, click on the '+' button)
* Settings page redesigned to rainbow icons list (as in Android Pie)
* Added "Fox Addons" page for OrangeFox's ZIP installer addons
* Added new beautiful page, if backups are not found
* Changed welcome message in log
* Fixed slider unlocking in lockscreen and changed to default
* We support now deep notches, statusbar will stretch as MIUI
* Added support for big round corners (Hello Poco)
* We maked optimisation to reduce lagging pages, keyboards and dialogs
* Also huge number of other changes and bug fixes
* Translations have been updated from our Crowdin pagee. Old translation have been removed.
