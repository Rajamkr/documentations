---
title: Installing OrangeFox Recovery
description: 
published: 1
date: 2020-04-29T17:17:21.957Z
tags: 
---

## How do I install OrangeFox?
+ To install an OrangeFox zip, flash it with any TWRP-compatible custom recovery without any wipes
+ To install an OrangeFox img, boot into fastboot mode, and run the command: `fastboot flash recovery OrangeFox-xyz.img`
+ See below for detailed instructions on how to install OrangeFox via fastboot

## Detailed instructions on how flash OrangeFox Recovery via fastboot

You need a PC for this. If you do not have a PC, or access to a PC, then you are stuck!
*Note - these instructions are for A-only devices*:

+ Install adb, fastboot [from here](https://developer.android.com/studio/releases/platform-tools#downloads), and the relevant USB drivers onto your PC
+ Install the Mi Flash tool onto your PC (only for Xiaomi devices)
+ Download the correct OrangeFox zip file to your phone, and to your PC
+ Extract recovery.img from the OrangeFox zip file, and copy recovery.img to your PC’s adb directory
+ Reboot your phone into fastboot/bootloader mode
+ Unlock your bootloader (skip if already done)
+ Open up a command line window / terminal emulator on your PC
+ Change to the adb directory on your PC 
+ Flash OrangeFox Recovery use `fastboot flash recovery recovery.img` command
+ Reboot in recovery by pressing vol+ + power keys till you will see OrangeFox splash (the keys may be may vary on your device)
+ After OrangeFox has booted up, check that everything is working – eg, that it has mounted the data partition successfully, and that the touchscreen works.
+ Find and select the OrangeFox zip, tap on it, and swipe to install it (because OrangeFox Recovery needs some files from the zip)
+ After installation, the phone will automatically reboot into OrangeFox
+ Enjoy!