---
title: MIUI / Flyme OTA
description: 
published: 1
date: 2020-04-29T17:26:11.130Z
tags: 
---

## How do I setup incremental MIUI OTA in OrangeFox Recovery?
Enabling incremental OTA updates in stock MIUI ROMs requires an intricate setup process, otherwise, it will not work. To set it up properly, take the following steps:
1. Download the most up-to-date release of OrangeFox Recovery
2. Download a full MIUI ROM, and copy it to your phone (normally this should be copied to an external storage device)
3. Flash the OrangeFox Recovery zip with your current custom recovery (or follow the steps in the thread for installing from adb/fastboot)
4. Reboot into OrangeFox Recovery (this will have happened automatically if you flashed the OrangeFox zip)
5. Go to the OrangeFox settings, tap on "MIUI OTA" and turn on "Enable MIUI OTA". If you have a recent stock MIUI 11 or MIUI 10 ROM that is based on Android Pie or Android 10, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings, otherwise the incremental MIUI OTA will <u>most definitely not work</u> - the typical symptoms of this failure are that, when you tap on "Reboot now" after downloading the incremental OTA update, **nothing** will happen - or you just get an error if you try to download the incremental OTA update. If you get this problem, you will need to repeat this entire process (starting with this step #5), with the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings.
6. Go to the “Wipe” menu, and wipe data, cache, and dalvik, and reboot OrangeFox
7. Flash your full MIUI ROM. After the ROM is flashed, OrangeFox will start the "OTA_BAK" process, which will backup your system (and sometimes vendor) and boot partitions into the /sdcard/Fox/OTA directory (this will be on your internal storage). <u>You must NOT delete any of the files in the /sdcard/Fox/OTA directory</u>. If you do, then incremental OTA updates will <u>most definitely</u> fail.
8. Reboot your phone, and start to use your phone normally
9. When MIUI notifies you that there is an update, download the update, using the MIUI updater, and allow it to reboot automatically to OrangeFox
10. OrangeFox will install the update automatically (this will take several minutes)
11. OrangeFox will reboot the phone automatically upon completion of the installation of the MIUI OTA update
12. After this, you will not need to flash a full ROM any more - just follow the steps in #9 above
13. Enjoy

NOTES: 
* If you want to move to a completely different version/build of MIUI, then you will first need to clean-flash the full ROM of that MIUI build, as described above.
* If you formatted your data partition before (or after) flashing MIUI, there is a bit of a rigmarole if you want incremental OTA support. See the note in the "<i>Returning to a MIUI ROM from a standard Treble ROM</i>" section below.


## How do I set up block-based incremental OTA for custom ROMs in OrangeFox Recovery?
* One of the best known examples of block-based incremental OTA is MIUI OTA (see immediately above).
* This kind of OTA update is now being introduced in custom ROMs. The set up process is identical to the set up process for MIUI incremental OTA
* This feature is new (*and experimental*) for custom ROMs, and it not enabled for all devices - so you first need to ascertain that the device's OrangeFox maintainer has enabled it in his build of OrangeFox. If it has not been enabled, then there is nothing that you can do.
* If you have ascertained that the device's OrangeFox maintainer has enabled this feature, then read on!
* Enabling incremental block-based OTA updates requires an intricate setup process, otherwise, it will not work. To set it up properly, take the following steps:

1. Download the custom ROM, and copy it to your phone (normally this should be copied to an external storage device)
2. Go to the OrangeFox settings, tap on "OTA" and turn on "Enable OTA".
3. [Optional] Go to the “Wipe” menu, and wipe data, cache, and dalvik, and reboot OrangeFox
4. Flash your ROM. After the ROM is flashed, OrangeFox will start the "OTA_BAK" process, which will backup your system (and sometimes vendor) images and boot partition into the /sdcard/Fox/OTA/ directory (this will be on your internal storage). <u>You must NOT delete any of the files in the /sdcard/Fox/OTA directory</u>. If you do, then incremental OTA updates will <u>most definitely</u> fail.
5. Reboot your phone, and start to use your phone normally
6. When ROM notifies you that there is an update, download the update, using the ROM's updater app, and allow it to reboot automatically to OrangeFox (**Note**: do not try to flash a block-based OTA zip manually from the recovery - this will **not** work - it is necessary to do all incremental OTA updates via the ROM's updater app)
7. OrangeFox will install the OTA update automatically (this can take several minutes)
8. OrangeFox will normally reboot the phone automatically upon completion of the installation of the OTA update
9. After this, you will should need to flash a full ROM any more - just follow the steps in #9 above
10. Enjoy


## Returning to a MIUI ROM from a Treble ROM (if your device has an unofficial/simulated Treble - for old devices that do not have a dedicated vendor partition)
+ "Simulated" Treble ROMs on many devices use MIUI’s “cust” partition for their vendor image. This process removes the Xiaomi proprietary files that are necessary for MIUI to run. These files must be restored, otherwise MIUI will not work properly.
+ For this purpose, the cleanest and easiest way to return to MIUI from a simulated Treble ROM is to flash a full fastboot MIUI ROM, using the Mi Flash tool.
+ If you did not create a backup of your MIUI cust partition to MicroSD or USB-OTG before installing a Treble ROM, then you must use the method just described above (or else you can try flashing a stock vendor image for your device (if there is one for your device there))
+ If you DID create a backup of your MIUI cust partition (in the newest versions of OrangeFox, this would be the “vendor” partition – it points to the same location as “cust”) to MicroSD or USB-OTG, then you can use OrangeFox to install MIUI when coming from a Treble ROM - but you have to take certain steps:
1. Backup your internal memory to an external device (eg, MicroSD, USB-OTG, or your PC). DO NOT SKIP THIS STEP!
2. Copy the latest stable MIUI ROM to your MicroSD or USB-OTG storage
3. Boot OrangeFox
4. Make sure that OrangeFox can read your MicroSD or USB-OTG storage, and can see the MIUI ROM that you copied there. This is the ROM that you will install in the steps below.
5. Select the “Wipe” menu and wipe everything - system, dalvik, cache, vendor, etc (except MicroSD/USB-OTG)
6. Format data (“format” - not “wipe”) - you will lose all the contents of your internal memory after doing this
7. Reboot OrangeFox - you will see a message saying “No OS installed …” - just swipe to reboot OrangeFox
8. Restore the backup of your MIUI cust partition (in the latest releases of OrangeFox, this will need to be restored to the "vendor" partition)
9. Flash your MIUI ROM
10. Reboot to system, and wait for a long time ...
11. When ready, restore your backup of your internal memory from your external storage device.

## Returning to a MIUI ROM from a standard Treble ROM
1. Bootup OrangeFpx
2. Backup your internal storage (user files, eg, photos, etc) to an external device
3. Wipe cache, dalvik, and system
4. Format data
5. Reboot OrangeFox (just swipe to continue if it complains that no ROM is installed)
6. For stock MIUI 10 or MIUI 11 ROM that is based on Android Pie or Android 10, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings. If that box is ticked, then incremental MIUI OTA will not work
7. Flash the MIUI ROM
8. Bootup the ROM (during the bootup process, MIUI will probably encrypt the data partition, unless you ticked "Disable Forced Encryption" before flashing MIUI; the encryption process will also wipe your internal storage)
9. Reboot to OrangeFox
10. Restore the backup of your internal storage

NOTE: if you want incremental OTA support for your stock MIUI ROM, then you must take the following steps after the stock MIUI ROM has booted for the first time after flashing it:
1. Reboot to OrangeFox
2. Make sure that all the MIUI OTA settings are enabled
3. If you have a stock MIUI 10 or MIUI 11 ROM that is based on Android Pie or Android 10, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings
4. Dirty-flash the MIUI ROM that you have just flashed (so that it will set up the OTA support)
5. Reboot to system
6. Configure your ROM


## Where are the settings for MIUI OTA?
Tap on *Menu*, and tap on the settings icon at the top right of the screen. You will see all the
various settings, including MIUI OTA.