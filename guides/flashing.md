---
title: Flashing
description: 
published: 1
date: 2020-04-30T17:39:12.030Z
tags: 
---

## Flashing ROMs
Flashing a ROM is usually a straightforward process. You can either do a "*clean*" flash, or a "*dirty*" flash. "Clean" flash is always recommended, but in some cases, like flashing ROM's update "dirty" flash will be fine.

> :warning: **Always make backups.**
Make a full backup of your current ROM - before performing any other operations in the recovery. At the very least, backup the /boot, /data, system_image (or system - if system_image is not available for backup) and vendor_image (or vendor - if vendor_image is not available for backup) partitions.
{.is-warning}

>:warning: **Xiaomi ARB**
Xiaomi have broken ARB implemention which can brick your phone forever!
If your Xiaomi device has ARB, and you are on a MIUI ROM, then you had better tread very carefully if you want to change your MIUI ROM to a different MIUI ROM, or to a different version of the same MIUI ROM. Search on Google for "ARB", read and understand everything, and then decide whether you really need to proceed.
{.is-warning}


### "Clean" flashing (Strongly recommended)
1. Bootup OrangeFox Recovery
2. Select "Wipe", and tick data, cache, and dalvik
3. Swipe to wipe (this will restore the installed ROM to a known state, and will remove apps/settings that might be incompatible with the ROM that you wish to install). 
4. Reboot OrangeFox
5. Select the ROM that you want to flash
6. Swipe to flash
7. Reboot OrangeFox recovery - before doing anything else - so that any changes to partitions/filesystems done by flashing the new ROM will take full effect
8. Flash whatever else you might want to flash (eg, GAPPs, magisk, etc)
9. Reboot your device
10. Wait for a long time while the new ROM sets itself up (go and make a cup of tea!)
11. Enjoy

### "Dirty" flashing (NOT recommended)
>:warning: Dirty flashing a ROM is **NOT** recommended. It can cause all kinds of random problems.
{.is-warning}
1. Bootup OrangeFox Recovery
2. Choose the ROM that you want to flash
3. Swipe to flash
4. Wipe cache and dalvik
5. Reboot OrangeFox recovery - before doing anything else - so that any changes to partitions/filesystems done by flashing the new ROM will take full effect
6. Flash whatever else you might want to flash (eg, GAPPs, magisk, etc)
7. Reboot your device (if it does not bootloop)
8. Enjoy (if the device actually manages to boot, and you do not get random errors)


## I want to "clean" flash a new ROM. What do I need to wipe before flashing?
+ At the very least, you should wipe *Dalvik/Art Cache, Cache, and Data*. 
+ Some ROM developers recommend also wiping System. Unless there is a **very specific reason** why you **really need** to do this, we do **not** recommend wiping System or Vendor.


## I want to flash something. How do I do select the file that I want to flash?
Tap on "*Files*", and navigate to the file that you want to flash. Tap on it, and you will see
the options for *flashing*. If you long-press on a file name, you will see a list of all the 
operations that you can perform on that file.


## I get an "error 7" when trying to flash (whatever)
+ The dreaded "error 7" can be caused by any number of things. Whatever is the specific cause in your case will be clearly displayed in red, and it will also be in the recovery log file. 
+ Usually, the proximate cause is a check that the author of your zip installer's updater-script has added whether it is for a specific firmware version, or some other specific feature. There will be an "assert" statement, which means that if the check fails, an error will be triggered.
+ Read very carefully what is on your screen and in the recovery log file, and address it. 
+ You may also want to [run a Google search](https://www.google.com/search?source=hp&ei=bt2nXp7cDJCPlwSrn6LgAQ&q=TWRP+error+7&oq=TWRP+error+7)
+ Do *not* ask for support, simply stating that you have got an error 7. You *must* provide details as to what is stated to be the cause, and *you must attach a copy of the recovery log*. If you do not do these, any request for support will simply amount to *noise*, and we will just ignore it.