---
title: Backups
description: 
published: 1
date: 2020-05-02T05:47:06.334Z
tags: 
---

## I want to make a backup of my ROM. Which partitions must I backup?
- At the very least, backup *<u>Boot</u>, <u>Data</u>, <u>System_Image</u> (or System - only if System_Image is not available for backup), and <u>Vendor_Image</u> (or Vendor - only if Vendor_Image is not available for backup)*. 
- You should also have at least one good backup of *<u>Persist_Image</u>*.
- Be sure to read this: https://forum.xda-developers.com/showpost.php?p=79771469&postcount=3


## Sometimes I get an "error 255" when trying to backup my data partition
+ This situation can happen for any number of reasons. Do a Google search for this.
+ The only way to ascertain the true cause is to check the recovery log file immediately, and search for the error. Normally the log would show where the error happened. 
+ Sometimes the error happens because of a file that is too large. If this is the case, then you will need to delete the file. If you really need to keep the file, then you are basically stuck.
+ Sometimes the error happens because of the present of a directory: `/data/per_boot` - in this case, you will need to remove the "/data/per_boot" directory. There are some other directories that may cause the same problem. The solution is the same - delete the directory before starting your backup.
+ Sometimes the problem happens because you have created multiple users on the ROM - in this case, you have to delete all the extra users
+ Sometimes the problem happens because you use parallel apps - in this case, you need to stop using that
+ If the problem is caused by something else, then you will need to report that to the device's maintainer - and *you must include the recovery log* in the report.


## I got an "error 255" when trying to restore my data backup
+ This is an old TWRP problem (do a Google search for this), which can be caused by a number of different things (eg, corrupted backups, or selinux policy issues, or multi-user or parallel apps stuff being backed up, etc, etc).
+ If you get this error while trying to restore a data partition backup, then you are stuck, as far as restoration via recovery is concerned. 
+ In that situation, your best option is to copy the backed up data files ("data.ext.win000", etc) to your PC, and then try to extract the files with a program that can extract "tar" archives (eg, "tar" on linux, "WinRAR" on Windows, etc). It may be possible to get access to many of your backed up files using this method. If this does not work, then there is nothing else that you can do.