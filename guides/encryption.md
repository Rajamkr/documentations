---
title: Encryption
description: 
published: 1
date: 2020-04-29T17:24:06.091Z
tags: 
---

## Will OrangeFox encrypt my phone?
+ No. OrangeFox (or any other recovery) **cannot** encrypt anything. Encryption is done by your **ROM**.
+ So, if you ever see anybody claiming that OrangeFox encrypted their phone, please tell them that this is <u>absolutely impossible</u> - because only a ROM can encrypt a phone.


## I am using OrangeFox, and now when I set a password or PIN in my ROM, they are rejected as being wrong
You will get this behaviour if you are using Lavender, and your device is not encrypted. This is the result of bugs in Lavender, which means that, if the device is not encrypted, then passwords/PIN/fingerprint will not work. The only solution at the moment is to encrypt your device. This is **not** an OrangeFox issue (so, please do not ask OrangeFox developers any questions about it). It is also not a ROM issue. It is a Xiaomi bug in Lavender.


## I am using OrangeFox, and now my fingerprint is not working/I can't find fingerprint settings
Same answer as above.


## I am using OrangeFox, and I am being asked for a password (or PIN). But I have not set a password (or PIN)
+ If your device has FBE encryption and you have set up a password/PIN in the ROM (or if it has FDE encryption, and you enabled Secure Startup in the ROM) then you will be asked for a password (or PIN, or whatever) - just supply the ROM's password/PIN.
+ If you did none of the above, then this is usually a sign of broken encryption (you may have flashed a ROM which has different encryption protocols from the ROM that you had before, or you may have flashed a firmware zip which uses an incompatible keymaster module - or whatever other reason). If this is the case, then you will need to <u>format your data partition</u> (which will also wipe all the contents of your internal storage). If you have no backups, then that would be most unfortunate.