---
title: Base things
description: 
published: 1
date: 2020-04-29T17:28:00.429Z
tags: 
---

## How do I configure the OrangeFox recovery?
Tap on *Menu*, and tap on the settings icon at the top right of the screen. You will see all the
various settings.


## How do I format data?
Go to *Menu -> Manage Partitions -> Data -> Format Data*. When you have finished formatting your data, reboot OrangeFox *before* doing anything else.


## How do I restore my ROM to default settings?
Tap on "*Wipe*", tick *Dalvik/Art Cache, Cache, Data*, and swipe to wipe. 


## Do I need to flash a "Mount system" zip when using OrangeFox?
No.


## Do I need to flash a "DM-Verity / Forced-Encryption" zip when using OrangeFox?
No.


## How do I take the OrangeFox logs for posting?
There are a variety of ways:
+ Tap on "*Menu -> More -> Copy Log to SD*", and swipe. This will copy the logs to your internal storage `/sdcard/Fox/`.
+ Connect a PC to your device via adb, and run the command: `adb pull /tmp/recovery.log`


## Aroma/AromaFM doesn't work
That is unfortunate - but there is nothing that can be done about it - unless and until somebody decides to update the Aroma project. This also applies to Aroma GAPPs.


## I have encountered a problem while using OrangeFox Recovery
+ Provide the logs and screenshots
+ Provide a full explanation of precisely what has happened, and precisely how you got to that point.
+ If you do not provide both of these, then we will simply ignore any report of any problem. So, it is advisable to (a) learn how to take logs before trying anything, and (b) always take the logs before rebooting after using the recovery.
+ Note: just posting a screenshot is totally unhelpful - and just saying that it "doesn't work" is totally unhelpful. You **must** post the recovery logs

## How can I know the maintainer for my device?
Tap on *Menu -> (Settings icon) -> About*

## How I can build OrangeFox for my own device?
[See this](/dev/building)