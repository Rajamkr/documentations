---
title: Entwicklung
description:
published: 1
date: 2020-03-10T21:12:58.397Z
tags:
---

+ [Erstellen](/dev/building)
+ [ Weg zum offiziellen Betreuer](https://wiki.orangefox.tech/en/dev/maintainerships)
+ [Richtlinien für offizielle Betreuer](/dev/maintainers_guidelines)
+ [API](/dev/api)
+ [OrangeFox-Infrastruktur](/dev/infrastructure)
+ [Themen-Entwicklung](/dev/theme_dev)
{.links-list}