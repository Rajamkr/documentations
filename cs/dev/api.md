---
title: OrangeFox Recovery API
description:
published: 1
date: 2020-04-26T18:08:01.083Z
tags:
---

> API v2 je uvolněno a připraveno k použití počínaje 26. dubnem 2020 API v1 bude zrušeno. Chcete-li zkontrolovat aktuální stav API v1, podívejte se na [API v1 stránku](v1) 
> 
> {.is-success}

## Stránky verze APL:
- [API v1](v1)
- [API v2](v2)
{.links-list}

## Příklad použití OrangeFox API v projektech:
*jméno | verze použitého API | jazyk*
- [OrangeFox Recovery Telegram bot (oBot) | v1 | Python](https://gitlab.com/OrangeFox/site/obot)
- [OrangeFox Recovery stažení webové stránky (dSite) | v2 | React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### Třetí strana
- [Přátelský modul uživatelského bota (FTG) | v1 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}








