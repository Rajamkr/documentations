---
title: Vývoj
description:
published: 1
date: 2020-03-10T21:12:58.397Z
tags:
---

+ [Stavba](/dev/building)
+ [ Cesta k oficiálnímu spravování](https://wiki.orangefox.tech/en/dev/maintainerships)
+ [Pokyny pro správce](/dev/maintainers_guidelines)
+ [API](/dev/api)
+ [Infrastruktura OrangeFox](/dev/infrastructure)
+ [Vývoj témat](/dev/theme_dev)
{.links-list}