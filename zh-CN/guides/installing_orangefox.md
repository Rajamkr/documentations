---
title: 安装 OrangeFox Recovery
description:
published: 1
date: 2020-04-29T17:17:21.957Z
tags:
---

## 如何安装 OrangeFox？
+ 如果想要通过 OrangeFox 压缩包安装 OrangeFox，您仅需要在任何与 TWRP 兼容的自定义 Recovery 中刷入该文件，无需任何清理。
+ 如果想要通过 OrangeFox 镜像文件安装，请先将手机启动到 fastboot 模式，然后输入指令: `fastboot flash recovery OrangeFox-xyz.img`
+ 请参阅下文通过 fastboot 刷入 OrangeFox 的详细说明

## 通过 fastboot 刷入 OrangeFox 的详细说明

您需要一台支持运行 ADB 的电脑(Windows 和 Linux 发行版都皆可)。 如果您电脑的操作系统是 Windows XP 或者更旧，您可能会遇到一点麻烦。 如果您没有一台支持 ADB 的电脑，或者不能使用任何支持 ADB 的电脑，您可能需要再费一番周折。您利用其他设备（比如手机）配置环境，完成下面的操作，但是这里并不对此进行介绍。 *注意，以下步骤仅适用于单 A 分区设备*:

+ Windows 用户请点击[此链接](https://developer.android.com/studio/releases/platform-tools#downloads)安装 ADB 和 fastboot，并且配置好相关的 USB 驱动。 如果您使用的是 Linux 发行版，您需要去您使用的 Linux 发行版的软件仓库寻找 ADB 和 fastboot 软件包并安装。
+ 如果您使用的是小米设备且电脑是 Windows 系统，请安装小米线刷工具
+ 下载与您设备型号相应的 OrangeFox 的压缩包到您的手机和您的电脑
+ Windows 用户请解压 OrangeFox 压缩包中的 recovery.img 文件至您电脑的 ADB 目录 如果您使用的是 Linux 发行版，您可以将该文件解压至任何你一个您可以找到的目录下
+ 将您的手机重启到 fastboot/bootloader 模式并连接电脑
+ 解锁您手机的 bootloader (如果已解锁，则跳过该步)
+ 在您的电脑上打开命令行窗口/终端
+ 更改目录为您电脑的 ADB 目录 (Linux 发行版用户可跳过此步骤)
+ 通过命令 `fastboot flash recovery recovery.img` 刷入 OrangeFox Recovery。 如果您使用的是 Linux 发行版，您需要以 root 权限执行该命令
+ 同时按住手机 音量+ 和 电源键 直到您看到 OrangeFox 第一屏 (对于某些设备，按键可能会发生变化)
+ OrangeFox 成功启动后，请检查一切是否正常工作。比如，分区是否成功挂载，屏幕是否能正常触控。
+ 找到并选中 OrangeFox 压缩包，点击，然后滑动刷入 (进行这一步是因为 OrangeFox Recovery 需要该压缩包中的某些文件)
+ 刷入完成后，手机会自动重启到 OrangeFox
+ Enjoy！