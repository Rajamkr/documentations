---
title: 基本事项
description:
published: 1
date: 2020-04-29T17:28:00.429Z
tags:
---

## 我应如何配置 OrangeFox Recovery？
点击 *菜单*，然后点击屏幕右上角的设置图标。 您将看到所有功能的设置。


## 我想要格式化 Data 该怎么做？
转到 *菜单 -> 分区管理 -> Data -> 格式化 Data*。 建议您在格式化 Data 后 *立即重启* OrangeFox。


## 我想将我的 ROM 恢复到默认设置该怎么做？
点击 "*清除*", 勾选 *Dalvik/Art Cache, Cache, Data*, 然后滑动以清除。


## 使用 OrangeFox 时，我需要刷入一个 "Mount system" 文件吗？
不需要。


## 使用 OrangeFox 时，我需要刷入一个 "DM-Verity / Forced-Encryption" 文件吗？
不需要。


## 我想要导出 OrangeFox 日志文件该怎么做？
这里有多种方式供您选择：
+ 点击 *菜单 -> 更多 -> 复制日志到 SD 卡*, 然后滑动. 这将复制日志到您的内置存储 (`/sdcard/Fox/`)。
+ 将您的设备通过 ADB 连接到电脑，输入命令：`adb pull /tmp/recovery.log`


## Aroma/AromaFM 不工作
遗憾的是，除非有人决定更新 Aroma 项目。这没有任何的解决办法。 Aroma GAPPs 同上。


## 我在使用 OrangeFox Recovery 时遇到了问题
+ 提供相关日志和截图
+ 请提供您面临的问题的详细信息，并描述清楚复现这一问题的步骤。
+ 如果你不提供上述两条信息，我们会无视您任何的问题反馈。 因此，最好(a) 在开始反馈前学习如何获取日志，(b)并在每次使用 Recovery 并重启前保存日志。
+ 注意：只提供屏幕截图是完全没有帮助的 - 只是描述 Recovery “不能工作”是完全没有帮助的。 您 **必须** 提供 Recovery 的日志

## 如何知道我的设备的维护者？
点击 *菜单 -> (设置图标) -> 关于*

## 如何为我自己的设备构建 OrangeFox ？
[请参阅这里](/dev/building)