---
title: 构建 OrangeFox
description:
published: 1
date: 2020-04-28T14:15:38.183Z
tags:
---

## 构建所需
+ 电脑/服务器
+ 合适的 Linux 环境 推荐使用 Ubuntu 18.04 和 Linux Mint 19.3 Arch Linux 上也可以正常工作，但是需要配置一些工作环境
+ 设备的 TWRP Tree

## OrangeFox 的初始版本
### 0. 准备构建的环境
``` bash
    cd ~
  sudo apt install git -y
  git clone https://github.com/akhilnarang/scripts
  cd scripts
  sudo bash setup/android_build_env.sh
  sudo bash setup/install_android_sdk.sh
```

### 1. 同步 OrangeFox 仓库源
``` bash
    mkdir OrangeFox
  cd OrangeFox
  repo init -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1
  repo sync -j8 --force-sync
```
***fox_8.1** 代表 OrangeFox 基于的 Android 版本，例如： fox_7.1, fox_8.1, fox_9.0*

提示: 使用 `repo init --depth=1 -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1` 进行浅度 clone 可以节约磁盘空间


### 2. 放置 tree 和 内核

您必须将您的 Device Tree 以及内核放在正确的位置。 例如 `device/xiaomi/mido`
``` bash
# 下面是示例命令
cd OrangeFox
mkdir -p device/xiaomi
cd device/xiaomi
git clone https://gitlab.com/OrangeFox/Devices/mido.git mido
```

### 3. 构建
``` bash
    cd OrangeFox
    source build/envsetup.sh
  export ALLOW_MISSING_DEPENDENCIES=true 
  export LC_ALL="C"
  lunch omni_<device>-eng && mka recoveryimage
```

### 4. 获取已构建的 OrangeFox
如果编译过程没有出现任何错误，最终生成的 Recovery 镜像将会存放于 out/target/product<device>/OrangeFox-unofficial-<device>.img

## 构建帮助
+ 如果您寻求为您的设备构建 OrangeFox 的帮助或支持，欢迎加入我们的 Telegram 群组 [OrangeFoxBuilding](https://t.me/OrangeFoxBuilding)  。

## 配置
OrangeFox 拥有许多它独有的构造方式，以便于开发者更好的控制 Recovery 的内置功能 -- [ 请点击这里 ](https://gitlab.com/OrangeFox/Vendor/blob/master/orangefox_build_vars.txt) 以便于您理解您应该启用哪些功能。


## 重新成为一个官方维护者

请参阅 [这里](/dev/maintainerships)
