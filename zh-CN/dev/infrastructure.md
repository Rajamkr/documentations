---
title: 基础设施
description:
published: 1
date: 2020-03-13T19:45:23.169Z
tags:
---

OrangeFox Recovery 的基础设施列表：

- Wiki： OrangeFox Wiki，运行于 [wiki.orangefox.tech](https://wiki.orangefox.tech)，[开源地址](https://wiki.js.org/)
- 邮箱：邮件服务器，运行于[mail.orangefox.tech](mail.orangefox.tech), [开源地址](https://poste.io)
- fox-h5ai: 文件列表, 运行于 [files.orangefox.tech](https://files.orangefox.tech), [开源地址](https://gitlab.com/OrangeFox/site/h5ai-dockered)
- oFilesServer: 静态文件服务器, 运行于 [files.orangefox.tech](https://files.orangefox.tech), 闭源
- mPanel: 维护人员面板，用于添加/编辑设备和发布, 运行于 [mpanel.orangefox.tech](https://mpanel.orangefox.tech), 闭源
- oAPI: API 服务器, 运行于 [api.orangefox.tech](https://api.orangefox.tech), [API documentation](/dev/api), 闭源
- oBot: Telegram 聊天机器人 ([@ofoxr_bot](https://t.me/ofoxr_bot)), [开源地址](https://gitlab.com/OrangeFox/site/obot)
- oSite: OrangeFox Recovery 网站, 运行于 [orangefox.tech](https://orangefox.tech), 闭源
{.grid-list}