---
title: OrangeFox Recovery API
description:
published: 1
date: 2020-04-26T18:08:01.083Z
tags:
---

> API v2 已发布，并准备从2020年4月26日开始使用。 API v1 将被放弃。 如果要检查 API v1 的当前状态，请参阅 [API v1页面](v1) 
> 
> {.is-success}

## API 版本页面：
- [API v1](v1)
- [API v2](v2)
{.links-list}

## 项目中使用 OrangeFox API 的示例：
*名称 | 使用的 API 版本 | 语言*
- [OrangeFox Recovery Telegram 机器人(oBot) | v1 | Python](https://gitlab.com/OrangeFox/site/obot)
- [OrangeFox Recovery 下载网站 (dsite) | v2 | React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### 第三方
- [Friendly telegram userbot module (FTG) | v1 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}








