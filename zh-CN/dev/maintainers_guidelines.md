---
title: 官方维护者指南
description:
published: 1
date: 2020-04-16T13:01:56.291Z
tags:
---

身为一名 OrangeFox 维护者，您需要肩负与光荣相应的职责。 以下是您应该遵循的简单的规则。
> :warning: 这些规则面向 OrangeFox 的官方维护者。 如果您想申请成为官方维护者，请参照 [本指南](/dev/maintainerships) 
> 
> {.is-warning}

> 这些规则适用于 OrangeFox R10.1，在未来版本里，规则还会不断更改。 
> 
> {.is-info}


### 正式版本
#### 构建类型
所有正式版的名字都应该含有关于构建类型的字段，诸如 `Stable`， `Beta` 或 `Unrested` (区分大小写)，build 将被视为非正式版。
#### 版本
您无权更改 OrangeFox Recovery 版本。 但是您可以自定义维护者的发布版本。 例如： `TW_DEVICE_VERSION=R10.1_1` 中， 'R10.1' 是 OrangeFox 版本号, '_1' 是维护者的发布版本号, 每当您发布一个新的版本，您都应该在维护者的发布版本号上加 1。
> :warning: 您不能使用同一个 TW_DEVICE_VERSION 发布多个更新！ 
> 
> {.is-warning}


### 正确地进行测试
这是最关键的部分。 每个发布的版本都应该是已正确测试过的。
* 每个将要发布的版本都必须使用 [test suite](/dev/building#test-suite) 进行测试。
* 测试版不必通过 test suite，但是它必须能正常启动，基本功能必须能正常工作。 并且它们必须支持刷入其他 OrangeFox 压缩包 和 镜像文件 (如果 Beta 版本出现问题，则回滚到稳定版本)。

### 核心基本规则
无论在哪台设备上，每个 OrangeFox 版本都必须保持相同的外观和行为。 因此，所有维护者都应遵守这些基本规则：

#### Fstab
A. MicroSD (如果设备支持)，必须在 fstab 中挂载为“/sdcard1”
+ 例如： /sdcard1 vfat /dev/block/mmcblk1p1 /dev/block/mmcblk1 flags=fsflags=utf8;display="MicroSD";storage;wipingui;removable

B. fstab 必须提供内置存储备份 (挂载为“/storage”)
+ 例如： /storage ext4 /data/media/0 flags=display="Internal Storage";usermrf;backup=1;fsflags="bind";removable

C. system_image 和 vendor_image 分区在 fstab 中都应有 "backup=1" 标签。
+ 例如: /system_image emmc /dev/block/bootdevice/by-name/system flags=backup=1;flashimg=1 /vendor_image emmc /dev/block/bootdevice/by-name/vendor flags=backup=1;flashimg=1

#### OrangeFox 变量
**变量列表请参照 [此处](https://gitlab.com/OrangeFox/Vendor/blob/master/orangefox_build_vars.txt)**
+ 如果您的设备是非标准分辨率(即不是1921x1080)，您应该导出 `Of_SCREEN_H`
+ 如果您的设备有圆角、切口或缺口，则应从列表中导出正确的变量。 不允许将偏移量设置为缺口以下！
+ 导出 `FOX_USE_NANO_EDITOR=1` 如果您的 Recovery 分区有足够的空间

### 仓库源
未经开发者的许可，您无权更改 OrangeFox 仓库源为发行版本或 Beta 版本。

### Tree 的维护
您必须在我们的 [GitLab](https://gitlab.com/OrangeFox) 界面维护一个设备或内核的 Tree。

#### 分支命名
设备/内核仓库应含有默认的 fox_(Android 版本) 分支 - 例如fox_9.0。 它应已经与最新版本同步，且，如果您正在处理新版本，您应该把它 push 到 master 分支。

### 持续更新
您的设备应该收到最新的 OrangeFox 版本更新和重要的提交。 并且，您应该从 TWRP 或 TWRP's forks projects trees 中挑选最新的 tree 进行修复/改进。

### 支持
您应该在我们的 [ Telegram 群组](https://t.me/OrangeFoxChat) 上为使用您发布的版本的用户提供支持。 XDA 上也应同样如此。

### 测试即将发布的版本
强烈建议在我们已关闭的仓库中进行测试。 为了能够访问已关闭的仓库，您应在您的 GitLab 配置文件中启用 2FA。

> :warning: 请务必妥善保管您 GitLab 的 2FA 密匙。 备份 2FA 密匙永远是个好的办法。 
> 
> {.is-warning}


## 维护一个新的设备
首先，您应该遵循此页面上所有的其他规则。 否则，您不会被许可维护另一个设备，因为您还不能正确地处理并支持当前设备。