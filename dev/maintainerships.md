---
title: Road to be an official maintainer
description: 
published: 1
date: 2020-03-10T22:36:18.763Z
tags: 
---

## Requirements 
+ Telegram and GitLab account
+ Experience of using GNU/Linux or macOS for some time
+ Having a device in hand
+ Supporting own unofficial builds
+ Not to be afraid of testing, much testing:
Testing new releases is the most important part of maintaining OrangeFox Recovery. Every new stable build should be tested and guaranteed to be working.
+ You shouldn't have a negative reputation in the Android community
+ You shouldn't be rude.
+ Hands and brains
+ Responsibility

## Steps
For first you should properly test your unofficial build and fix all known bugs. Our test suite:
### Test suite
#### Base functional
1. Installing .zip files work
2. Installing .img files work
3. Backup works
4. Backup encryption working:
New backup > lock icon. Working only if you have selected /data partition.
5. Restore works
6. The external SD card / OTG (if there is one) can be read
7. Backup to external SD card / OTG works
8. MIUI OTA works (on supported devices)
9. Built-in features work normally
10. Settings work and are kept after a reboot
11. OrangeFox Recovery can decrypt encrypted data partition without asking for a password if no passwords has been set in the ROM

#### Advanced functional
12. Recovery password protection works
13. Flashlight works (on supported devices)
14. AromaFM works (on supported devices)
15. Changing themes and splash work and are kept after reboot:
Changing splashes uses mkbootimg binary, so if the splash is not retained after rebooting, it would usually be related to this.


You should fix as many issues as you find. But if you can't fix some issues it won't be a reason for refusing your request for maintainership - but every release you must clearly document all known bugs.
Some advanced functionality is not critical. You can easily disable features that are not working via special build vars.

----

  
All maintainers should follow [maintainer's guidelies](/dev/maintainers_guidelines). It includes base rules about fstab and more. You should read it before proceeding.
  
Then you can ask [@MrYacha](https://t.me/MrYacha) (In Telegram) to get official maintainership. Please write if you meet these requirements.

Your next step will be asking for GitLab permissions and pushing your trees to the OrangeFox gitlab repo. Then you will be allowed to push a Beta release. Only after positive and successful beta testing will you be able to make a Stable release.

How will builds be released?
We have own maintainers panel website which will do all hard-work instead of you.