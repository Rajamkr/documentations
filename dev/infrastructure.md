---
title: Infrastructure
description: 
published: 1
date: 2020-03-13T19:45:23.169Z
tags: 
---

OrangeFox Recovery's infrastructure list:

- wikijs: OrangeFox Wiki, serving [wiki.orangefox.tech](https://wiki.orangefox.tech), [open-sourced](https://wiki.js.org/)
- posteio: Mail server, serving [mail.orangefox.tech](mail.orangefox.tech), [open-sourced](https://poste.io)
- fox-h5ai: File lister, serving [files.orangefox.tech](https://files.orangefox.tech), [open-sourced](https://gitlab.com/OrangeFox/site/h5ai-dockered)
- oFilesServer: Static files server, serving files on [files.orangefox.tech](https://files.orangefox.tech), closed-sourced
- mPanel: Panel for maintainers for add/edit devices and releases, serving [mpanel.orangefox.tech](https://mpanel.orangefox.tech), closed-sourced
- oAPI: API server, serving [api.orangefox.tech](https://api.orangefox.tech), [API documentation](/dev/api), closed-sourced
- oBot: Telegram chat-bot ([@ofoxr_bot](https://t.me/ofoxr_bot)), [open-sourced](https://gitlab.com/OrangeFox/site/obot)
- oSite: OrangeFox Recovery website, serving [orangefox.tech](https://orangefox.tech), closed-sourced
{.grid-list}