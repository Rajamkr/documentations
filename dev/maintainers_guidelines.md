---
title: Gudelines for official maintainers
description: 
published: 1
date: 2020-04-16T13:01:56.291Z
tags: 
---

Being an OrangeFox maintainer is a serious responsibility. Here you can find some simple rules which you should follow.
> :warning: These rules apply to OrangeFox's official maintainers. If you want to apply to become an official maintainer, [follow this guide](/dev/maintainerships)
{.is-warning}

> These rules relate to OrangeFox Recovery R10.1. For futures versions, the rules can be changed.
{.is-info}


### Official builds
#### Build type
All official builds should have a build type field, it should be `Stable`, `Beta` or `Untested` (case sensitive), build will be considered as unofficial.
#### Version
You aren't allowed to change OrangeFox Recovery version. But you can set maintainer's release. For example:
`TW_DEVICE_VERSION=R10.1_1` where 'R10.1' is OrangeFox version, '_1' is a maintainer's release, by every new OrangeFox build with same version you should increase maintainer's release on 1.
> :warning: You cannot release more than one build with the same TW_DEVICE_VERSION!
{.is-warning}


### Proper testing
This is the most important point. Every build should be properly tested.
* Every release build must be tested with [this](/dev/building#test-suite) test suite. 
* Beta builds do not have to pass the test suite - but they must boot correctly, basic features must work, and they must support flashing other OrangeFox Installation zips and imgs (for rolling back to a stable version if something is broken in the beta).

### Core ground rules
Every OrangeFox release must look and behave the same way, no matter the device. Therefore all maintainers should follow these ground rules:

#### Fstab
A. The MicroSD (if the device supports it) must be mounted in fstab as "/sdcard1"
+ Example: 
/sdcard1 vfat /dev/block/mmcblk1p1 /dev/block/mmcblk1 flags=fsflags=utf8;display="MicroSD";storage;wipeingui;removable

B. The fstab must provide for backup of internal storage (mounted as "/storage")
+ Example:
/storage ext4 /data/media/0 flags=display="Internal Storage";usermrf;backup=1;fsflags="bind";removable

C. The system_image and vendor_image partitions should have the "backup=1" flag in fstab
+ Examples:
/system_image emmc /dev/block/bootdevice/by-name/system flags=backup=1;flashimg=1
/vendor_image emmc /dev/block/bootdevice/by-name/vendor flags=backup=1;flashimg=1

#### OrangeFox variables
**The variables list is [here](https://gitlab.com/OrangeFox/Vendor/blob/master/orangefox_build_vars.txt)**
+ If your device has a non-standard resolution (i.e., it is not 1921x1080) you should export `OF_SCREEN_H`
+ If your device has round corners, cutouts, or notches, you should export the correct vars from the list. Setting offset to drop notch space is not allowed!
+ export `FOX_USE_NANO_EDITOR=1` if your recovery partition has sufficient space

### Sources
You cannot change OrangeFox sources for a release or beta build without prior agreement with the developers.

### Trees maintaining
You should maintain a device/kernel tree on [our GitLab page](https://gitlab.com/OrangeFox).

#### Branches naming
The device/kernel repository should have default a fox_(android version) branch - for example, fox_9.0. It should have been synced with most up-to-date release source, and, if you are working on a new release you should push it in master.

### Keep updated
Your device should receive the latest OrangeFox releases and important commits. Also, you should pick the latest tree fixes/improvements from TWRP or TWRP's forks projects trees.

### Support
You should provide your build's users with support on [our Telegram chat](https://t.me/OrangeFoxChat). XDA is also highly recommended.

### Testing upcoming releases
Testing upcoming releases from our closed repository are highly recommended. In order to be able to access the closed repository, you should have enabled 2FA on your GitLab profile.

> :warning: Please don't loose your GitLab's 2FA keys. Backuping 2FA keys is always good idea.
{.is-warning}


## Maintaining a new device
For first you should follow all the other rules on this page. Otherwise, you will not be allowed to maintain another device, as you cannot yet handle and support the current one(s) properly.