---
title: Building OrangeFox
description: 
published: 1
date: 2020-04-28T14:15:38.183Z
tags: 
---

## Requirements
+ PC / Server
+ Proper linux environment. Ubuntu 18.04 or Linux Mint 19.3 is recommended. ArchLinux is working fine but it needs some workarounds.
+ TWRP tree for device

## Initial build of OrangeFox
### 0. Prepare the build environment
``` bash
	cd ~
  sudo apt install git -y
  git clone https://github.com/akhilnarang/scripts
  cd scripts
  sudo bash setup/android_build_env.sh
  sudo bash setup/install_android_sdk.sh
```

### 1. Sync OrangeFox sources
``` bash
	mkdir OrangeFox
  cd OrangeFox
  repo init -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1
  repo sync -j8 --force-sync
```
***fox_8.1** means the Android version on which OrangeFox based, there are: fox_7.1, fox_8.1, fox_9.0*

Tip: Use `repo init --depth=1 -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1` to initialize a shallow clone to save disk space


### 2. Place trees and kernel

You have to place your device trees, kernels in the proper locations.
For example `device/xiaomi/mido`
``` bash
# This is an example commands
cd OrangeFox
mkdir -p device/xiaomi
cd device/xiaomi
git clone https://gitlab.com/OrangeFox/Devices/mido.git mido
```

### 3. Build it
``` bash
	cd OrangeFox
	source build/envsetup.sh
  export ALLOW_MISSING_DEPENDENCIES=true 
  export LC_ALL="C"
  lunch omni_<device>-eng && mka recoveryimage
```

### 4. Take OrangeFox build
If there weren't any errors during compilation, the final recovery image would be present in out/target/product/<device>/OrangeFox-unofficial-<device>.img

## Help with building
+ If you want help/support with respect to building OrangeFox for your device, go to our [OrangeFoxBuilding](https://t.me/OrangeFoxBuilding) telegram group. 

## Configs
OrangeFox has many of its own configurations, which give developers fine control over the features that are built into the recovery - [see this](https://gitlab.com/OrangeFox/Vendor/blob/master/orangefox_build_vars.txt) to understand which you should to enable.
  
  
## Road to be an official maintainer
  
Read [here](/dev/maintainerships)
