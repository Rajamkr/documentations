---
title: OrangeFox Recovery API
description: 
published: 1
date: 2020-04-26T18:08:01.083Z
tags: 
---

> API v2 is released and ready to use starting from 26 April 2020
API v1 will be abandoned. To check current status of API v1 see [API v1 page](v1)
{.is-success}

## API versions pages:
- [API v1](v1)
- [API v2](v2)
{.links-list}

## Example of usage OrangeFox API in projects:
*name | version of used API | language*
- [OrangeFox Recovery Telegram bot (oBot) | v1 | Python](https://gitlab.com/OrangeFox/site/obot)
- [OrangeFox Recovery downloads website (dSite) | v2 | React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### Third-party
- [Friendly telegram userbot module (FTG) | v1 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}








