---
title: OrangeFox changelogs
description:
published: 1
date: 2020-03-05T17:42:56.032Z
tags:
---

## OrangeFox R11
> In fase di forte sviluppo. Stimato per il 2020 
> 
> {.is-info}
* Linee guida di progettazione per Android Q (10)
* Android Q gestures
* UX migliore
* Miglioramenti e correzioni di Magisk manager
* Ora l'icona della batteria è disegnata usando codice C
* Modifiche alla TWRP incorporate

## OrangeFox R10.1 - Versione più recente
* Salva lo storico dei logs (zip compresso) - nella cartella /sdcard/Fox/logs/
* Ora tutti i log verranno inseriti in /sdcard/Fox/logs/
* Aggiunto il supporto per MIUI OTA incrementale su dispositivi più recenti (es., lavender, violet)
* Supporto migliorato per il sistema rootati
* Supporto migliorato per Android 10
* Supporto migliorato per MIUI 11
* L'impostazione predefinita per le caselle "Disabilita DM-Verity" e "Disabilita la crittografia forzata" è quella di deselezionarle. Questo è necessario perché, all'inizio, le ultime ROM MIUI in stock basate su **Android Pie** o **Android 10** non sono affatto felici di disabilitare DM"-Verity" (gli aggiornamenti incrementali dell'OTA di MIUI non funzioneranno, e potrebbero esserci altri problemi); in secondo luogo, le recenti ROM MIUI possono crittografare comunque il tuo dispositivo e ignorare tutte le impostazioni per fermare questo comportamento; in terzo luogo, alcune recenti ROM basate su AOSP non sono affatto contente di disabilitare "DM-Verity" o la cifratura forzata. Pertanto, chiunque scelga di abilitare queste impostazioni deve spuntare manualmente quelle caselle prima di flashare le ROM (i manutentori per dispositivi molto vecchi potrebbero aver mantenuto le impostazioni predefinite originali, perché i vecchi dispositivi con MIUI basati su versioni precedenti di Android non hanno questo problema)
* L'addon Magisk è stato aggiornato (ora Magisk v20.1)
* Gestisci i messaggi "Vip Android"
* Informazioni più dettagliate sulla ROM installata (se presente)
* Armonizza alcune fonti con twrp stock
* Crittografia: non provare la chiave wrapped se non necessaria
* Crittografia: cryptf: aggiunto il supporto per keymaster 2
* Risolti problemi con il gestore applicazioni
* Disabilitati alcuni avvisi non necessari per le Rom personalizzate
* Aggiunte la lingua ucraina, francese e vietnamita
* Aggiornate tutte le altre lingue
* Molte piccole correzioni

## OrangeFox R10
* Design completamente aggiornato alle linee guida del Material Design 2
* Il team OrangeFox ha ripensato la logica del recupero
* Sono stati uniti gli ultimi commit della TWRP 3.1
* Aggiunto gestione app ROM integrato
* Aggiunto Magisk Manger (non più bootloops con moduli errati!)
* Codice Flashlight aggiornato per supportare più dispositivi
* Aggiunta opzione di backup rapido (per il backup delle partizioni essenziali con un click)
* Aggiunto il supporto per dispositivi OTA non MIUI
* MIUI OTA aggiornato con molte correzioni
* Aggiunta opzione di sicurezza codice PIN
* Opzione di sicurezza per le gesture
* Migliorata notevolmente la persistenza della protezione delle OrangeFox. La password non verrà reimpostata neanche dopo aver reinstallato OrangeFox.
* Aggiunta la possibilità di bloccare ADB all'avvio.
* Aggiornamenti significativi al File Manager integrato
* Il gestore file supporta il tasto "indietro" per andare nelle directory padre
* L'installazione di ZIP uniti con File Manager
* Aggiunto l'ordinamento ZIP, IMG per i file nel gestore file
* Per vedere le azioni disponibili per un file, premilo a lungo
* File Manager mostra il percorso della directory corrente
* Ricerca aggiunta nel gestore file
* Aggiunta la capacità di lettura dei file txt nel File Manager
* Aggiunta la possibilità di comprimere/spacchettare nel File Manager
* Aggiunto checker Checksum nel File Manager
* Aggiunta pagina di informazioni nell'azione file
* Aggiunta la possibilità di impostare premissioni nel File Manager
* Aggiunto supporto per tasti di navigazione hardware
* Vol+ e tasto accensione per attivare la torcia
* Aggiunta opzione di riavvio EDL
* Rimosso codice rigido MIUI OTA, possiamo buildare OrangeFox senza dispositivi non-Xiaomi
* Aggiunti un sacco di personalizzazioni, nuovi temi, colori, impostazioni, stili della batteria, impostazioni della barra di navigazione, e molto altro.
* I temi ora si applicano più velocemente e si mantengono dopo la reinstallazione di OrangeFox
* Formatta dati e partizione di SD uniti nella nuova pagina "Gestisci partizioni".
* Il menu "Wipe" contiene solo azioni di cancellazione e non altro.
* Per avviare AromaFM devi cliccare due volte (niente più apertura accidentale di AromaFM)
* Backup e ripristino uniti in una pagina (per aggiungere backup, fare clic sul pulsante '+')
* Pagina delle impostazioni ridisegnata per l'elenco delle icone arcobaleno (come in Android Pie)
* Aggiunta la pagina "Fox Addons" per gli addons ZIP di OrangeFox
* Aggiunta una nuova schermata elegante, se non vengono trovati i backup
* Messaggio di benvenuto cambiato nel log
* Corretto lo sblocco dello slider nella schermata di blocco e cambiato a predefinito
* Supportiamo ora le notifiche profonde, la barra di stato si estenderà fino come la MIUI
* Aggiunto supporto per i grandi angoli rotondi (Hello Poco)
* Abbiamo fatto delle ottimizzazioni per ridurre lag nelle pagine, nelle tastiere e nelle finestre di dialogo
* Anche un gran numero di altri cambiamenti e correzioni di bug
* Le traduzioni sono state aggiornate dalla nostra pagina Crowdin. La vecchia traduzione è stata rimossa.
