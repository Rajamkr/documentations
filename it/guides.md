---
title: Guide e FAQ
description:
published: 1
date: 2020-04-29T20:18:56.154Z
tags:
---

- [Installare OrangeFox Recovery](installing_orangefox)
- [Domande basiche](base)
- [Flashing](flashing)
- [Backup](backups)
- [MIUI / Flyme OTA](ota)
- [Crittazione](encryption)
- [Altro](others)
{.links-list}

