---
title: Crittografia
description:
published: 1
date: 2020-04-29T17:24:06.091Z
tags:
---

## OrangeFox crittograferà il mio telefono?
+ No. OrangeFox (o qualsiasi altra recovery) **non può** crittografare nulla. La crittografia è fatta dalla tua **ROM**.
+ Quindi, se vedrai mai qualcuno sostenere che OrangeFox crittografa il tuo telefono, si prega di dire che questo è <u>assolutamente impossibile</u> - perché solo una ROM può crittografare un telefono.


## Sto usando OrangeFox, e mentre inserisco la password o il PIN nella mia ROM, vengono rifiutati e considerati sbagliati
Otterrai questo comportamento se stai utilizzando Lavender, e il tuo dispositivo non è crittografato. Questo è il risultato di un bug in Lavender, il che significa che, se il dispositivo non è crittografato, le password/PIN/impronta digitale non funzioneranno. L'unica soluzione al momento è crittografare il tuo dispositivo. Questo **non è** un problema di OrangeFox (quindi, per favore non chiedere domande agli sviluppatori di OrangeFox a riguardo). Non si tratta neppure di un problema della ROM. È un bug di Xiaomi nella Lavender.


## Sto usando OrangeFox, e ora la mia impronta digitale non funziona/Non riesco a trovare le impostazioni dell'impronta digitale
Stessa risposta come sopra.


## Sto usando OrangeFox, e mi viene chiesta una password (o PIN). Ma non ho impostato una password (o PIN)
+ Se il tuo dispositivo ha la crittografia FBE e hai impostato una password/PIN nella ROM (o se ha la crittografia FDE, e hai abilitato l'avvio sicuro nella ROM), ti verrà richiesta una password (o PIN, o qualsiasi altra cosa) - inserire semplicemente la password/PIN della ROM.
+ Se non hai fatto nessuna delle precedenti, allora questo potrebbe essere un segno di crittografia non funzionante (potresti aver flashato una ROM che ha protocolli di crittografia diversi dalla ROM che avevi prima, o potresti aver flashato un firmware zip che utilizza un modulo keymaster incompatibile - o qualsiasi altra ragione). Se è così, poi dovrai <u>formattare la tua partizione di dati</u> (che cancellerà anche tutti i contenuti della tua memoria interna). Se non avete nessu backup, allora siete sfortunati.