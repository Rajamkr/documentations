---
title: Elementi base
description:
published: 1
date: 2020-04-29T17:28:00.429Z
tags:
---

## Come posso configurare OrangeFox Recovery?
Tocca *Menu* e poi tocca l'icona delle impostazioni in alto a destra dello schermo. Vedrai tutte le varie impostazioni.


## Come posso formattare i dati?
Vai a *Menu -> Gestisci partizioni -> Dati -> Formattazione dati*. Quando hai finito di formattare i tuoi dati, riavvia OrangeFox *prima di* fare altro.


## Come posso ripristinare la mia ROM alle impostazioni predefinite?
Tocca "*Cancella*", spunta *Dalvik/Art Cache, Dati*, e scorri per cancellare.


## È necessario flashare uno zip "Mount system" quando si usa OrangeFox?
No.


## È necessario flashare uno zip "DM-Verity / Forced-Encryption" quando si utilizza OrangeFox?
No.


## Come faccio a prendere i log di OrangeFox per postare?
Ci sono diversi modi:
+ Tocca*Menu -> Altro -> Copia log nella SD*", e scorri. Questo copierà i log nella memoria interna `/sdcard/Fox/`.
+ Collega un PC al tuo dispositivo tramite adb, ed esegui il comando: `adb pull /tmp/recovery.log`


## Aroma/AromaFM non funziona
E' un peccato - ma non c'è nulla da fare - a meno che e fino a quando qualcuno non deciderà di aggiornare il progetto Aroma. Questo vale anche per Aroma GAPPs.


## Ho riscontrato un problema usando OrangeFox Recovery
+ Fornisci i log e gli screenshot
+ Fornite una spiegazione completa di ciò che è accaduto ed esattamente come siete arrivati a questo punto.
+ Se non li fornirete entrambi, ci limiteremo a ignorare qualsiasi segnalazione di problemi. Quindi, è consigliabile (a) imparare come prendere i log prima di provare qualsiasi cosa, e (b) prelevare sempre i log prima di riavviare dopo aver utilizzato il ripristino.
+ Nota: postare solo uno screenshot è del tutto inutile - e dire semplicemente che "non funziona" è del tutto inutile. **devi** inviare i log di recupero

## Come posso conoscere il manutentore del mio dispositivo?
Tocca *Menu -> (Icona impostazioni) -> Informazioni*

## Come posso buildare OrangeFox per il mio dispositivo?
[Vedi questo](/dev/building)