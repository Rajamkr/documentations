---
title: Backup
description:
published: 1
date: 2020-04-29T17:25:19.204Z
tags:
---

## Voglio fare un backup della mia ROM. Quali partizioni devo salvare?
- Fai almeno il backup di *Boot, Data, System_Image (o Sistema - solo se System_Image non è disponibile per il backup), e Vendor_Image (o venditore - solo se Vendor_Image non è disponibile per il backup)*
- Assicurati di leggere questo: https://forum.xda-developers.com/showpost.php?p=79771469&postcount=3


## A volte ottengo un "errore 255" quando provo a fare un backup della partizione dei dati
+ Questa situazione può verificarsi per tutta una serie di motivi. Fai una ricerca Google per questo.
+ L'unico modo per accertare la vera causa è controllare immediatamente il file di log di recupero e cercare l'errore. Normalmente il log mostrerebbe dove si è verificato l'errore.
+ A volte l'errore accade a causa di un file troppo grande. Se così fosse, allora dovrai eliminare quel file. Se hai davvero bisogno di mantenere quel file, allora sei praticamente spacciato.
+ A volte l'errore accade a causa di questa directory: `/data/per_boot` - in questo caso, sarà necessario rimuovere la directory "/data/per_boot". Ci sono altre directory che potrebbero causare lo stesso problema. La soluzione è la stessa - elimina la cartella prima di iniziare il backup.
+ A volte il problema accade perché hai creato più utenti sulla ROM - in questo caso, devi eliminare tutti gli utenti aggiuntivi
+ A volte il problema si verifica perché usi applicazioni parallele - in questo caso, devi smettere di usare questa funzione
+ Se il problema è causato da qualcos'altro, dovrai segnalarlo al manutentore del dispositivo - e *devi includere il log di recupero* nel report.


## Ho ricevuto un "errore 255" nel tentativo di ripristinare il backup dei miei dati
+ Questo è un vecchio problema TWRP (fare una ricerca Google per questo), che può essere causato da una serie di cose diverse (es. backup corrotti, o problemi di politica selinux, o file multi-utente o applicazioni parallele in fase di backup, ecc.).
+ Se ottieni questo errore durante il tentativo di ripristinare un backup della partizione dei dati, sei spacciato, per quanto riguarda il ripristino tramite recovery.
+ In questa situazione, la migliore opzione è copiare i file di dati salvati ("data.ext. in000", ecc) nel tuo PC, quindi prova ad estrarre i file con un programma che può estrarre archivi "tar" (es. "tar" su linux, "WinRAR" su Windows, ecc). Potrebbe essere possibile accedere a molti dei file di backup utilizzando questo metodo. Se questo non funziona, allora non c'è niente che tu possa fare.