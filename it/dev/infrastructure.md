---
title: Infrastruttura
description:
published: 1
date: 2020-03-13T19:45:23.169Z
tags:
---

Elenco infrastrutture di OrangeFox Recovery:

- wikijs: OrangeFox Wiki, servendo [wiki.orangefox.tech](https://wiki.orangefox.tech), [open-sourced](https://wiki.js.org/)
- posteio: Mail server che serve [mail.orangefox.tech](mail.orangefox.tech), [open-sourced](https://poste.io)
- fox-h5ai: File lister, servendo [files.orangefox.tech](https://files.orangefox.tech), [open-sourced](https://gitlab.com/OrangeFox/site/h5ai-dockered)
- oFilesServer: server di file statici, servendo i file su [files.orangefox.tech](https://files.orangefox.tech), closed-sourced
- mPanel: pannello per manutentori per aggiungere/modificare dispositivi e rilasci, servendo [mpanel.orangefox.tech](https://mpanel.orangefox.tech), open-source
- oAPI: server API, servendo [api.orangefox.tech](https://api.orangefox.tech), [documentazione API](/dev/api), closed-source
- oBot: bot Telegram ([@ofoxr_bot](https://t.me/ofoxr_bot)), [open-source](https://gitlab.com/OrangeFox/site/obot)
- oSite: sito web di recupero OrangeFox, al servizio di [orangefox.tech](https://orangefox.tech), origine chiusa
{.grid-list}