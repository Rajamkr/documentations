---
title: OrangeFox Recovery API
description:
published: 1
date: 2020-04-26T18:08:01.083Z
tags:
---

> API v2 è stato rilasciato e pronto all'uso a partire dal 26 Aprile 2020 API v1 verrà abbandonata. Per controllare lo stato attuale di API v1 vedi [API v1 pagina](v1) 
> 
> {.is-success}

## Lista versioni API:
- [API v1](v1)
- [API v2](v2)
{.links-list}

## Esempio di utilizzo delle API OrangeFox nei progetti:
*nome a <unk> version dell'API utilizzata <unk> lingua*
- [OrangeFox Recovery Telegram bot (oBot) | v1 | Python](https://gitlab.com/OrangeFox/site/obot)
- [Scaricamento del sito di recupero OrangeFox (dSite) | v2 | React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### Terze parti
- [Modulo Friendly Telegram userbot (FTG) | v1 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}








