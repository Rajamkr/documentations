---
title: Creazione OrangeFox
description:
published: 1
date: 2020-04-28T14:15:38.183Z
tags:
---

## Requisiti
+ PC / Server
+ Un ambiente linux corretto. Ubuntu 18.04 o Linux Mint 19.3 sono raccomandati. ArchLinux sta funzionando bene, ma ha bisogno di alcune soluzioni alternative.
+ Albero TWRP per il dispositivo

## Build iniziale di OrangeFox
### 0. Prepara l'ambiente per la build
``` bash
    cd ~
  sudo apt install git -y
  git clone https://github.com/akhilnarang/script
  cd script
  sudo bash setup/android_build_env.sh
  sudo bash setup/install_android_sdk.sh
```

### 1. Sincronizza sorgenti OrangeFox
``` bash
    mkdir OrangeFox
  cd OrangeFox
  repo init -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1
  repo sync -j8 --force-sync
```
***fox_8.1** indica la versione di Android su cui si basa OrangeFox, ci sono: fox_7.1, fox_8.1, fox_9.0*

Suggerimento: Utilizzare `repo init --depth=1 -u https://gitlab.com/OrangeFox/Manifest.git -b fox_8.1` per inizializzare un clone poco profondo per salvare lo spazio su disco


### 2. Posiziona trees e kernel

Devi posizionare i trees del tuo dispositivo e i kernel nelle posizioni corrette. Per esempio `device/xiaomi/mido`
``` bash
# Questo è un esempio dei comandi
cd OrangeFox
mkdir -p device/xiaomi
cd device/xiaomi
git clone https://gitlab.com/OrangeFox/Devices/mido.git mido
```

### 3. Buildare
``` bash
    cd OrangeFox
    source build/envsetup.sh
  export ALLOW_MISSING_DEPENDENCIES=true 
  export LC_ALL="C"
  pranzo omni_<device>-eng && mka recoveryimage
```

### 4. Prendi la build OrangeFox
Se non si verificassero errori durante la compilazione, l'immagine di recupero finale sarebbe presente in out/target/product/<device>/OrangeFox-unofficial-<device>.img

## Aiuto per la build
+ Se vuoi aver aiuto/supporto per quanto riguarda la costruzione di OrangeFox per il tuo dispositivo, vai sul nostro gruppo Telegram [OrangeFoxBuilding](https://t.me/OrangeFoxBuilding).

## Configurazioni
OrangeFox ha molte delle sue configurazioni, che dà agli sviluppatori un buon controllo sulle funzionalità che sono integrate nella recovery - [vedi questo](https://gitlab.com/OrangeFox/Vendor/blob/master/orangefox_build_vars.txt) per capire cosa si dovrebbe abilitare.


## Come diventare un manutentore ufficiale

Leggi [qui](/dev/maintainerships)
